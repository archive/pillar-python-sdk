#!/usr/bin/env bash

# Updates the SDK version in the right places.
if [ -z "$1" ]; then
    echo "Usage: $0 version" >&2
    exit 1
fi

VERSION="$1"

poetry version "$VERSION"
sed "s/__version__ = .*/__version__ = '$VERSION'/" -i pillarsdk/config.py

git diff
echo
echo "Don't forget to commit and tag:"
echo git commit -m \'Bumped version to $1\' pyproject.toml pillarsdk/config.py
echo git tag -a v$1 -m \'Tagged version $1\'
