import unittest
import responses

import pillarsdk
import pillarsdk.exceptions as sdk_exceptions

mock = responses.RequestsMock(assert_all_requests_are_fired=True)


class AssetNodesTests(unittest.TestCase):
    def setUp(self):
        self.endpoint = 'http://localhost:12345'
        self.api = pillarsdk.Api(
            endpoint=self.endpoint,
            username='',
            password='',
            token='jemoeder',
        )
        self.project_id = 1234

    @mock.activate
    def test_create_asset_from_file__always_new_node(self):
        parent_node_id = 24 * 'a'

        mock.add(responses.POST,
                 '%s/nodes' % self.endpoint,
                 json={
                     '_id': 24 * 'b',
                     'parent': parent_node_id,
                     'name': 'test_nodes.py',
                     'project': self.project_id,
                 },
                 status=201)

        mock.add(responses.POST,
                 '%s/storage/stream/%s' % (self.endpoint, self.project_id),
                 json={
                     'status': 'ok',
                     'file_id': 24 * 'c',
                 },
                 status=201)

        node = pillarsdk.Node.create_asset_from_file(
            project_id=self.project_id,
            parent_node_id=parent_node_id,
            asset_type='image',
            filename=__file__,
            always_create_new_node=True,
            api=self.api)

        self.assertEqual(node['_id'], 24 * 'b')
        self.assertEqual(node['parent'], parent_node_id)
        self.assertEqual(node['name'], 'test_nodes.py')
        self.assertEqual(node['node_type'], 'asset')
        self.assertEqual(node['properties']['content_type'], 'image')
        self.assertEqual(node['properties']['file'], 24 * 'c')

    @mock.activate
    def test_create_asset_from_file__update_existing_node(self):
        parent_node_id = 24 * 'a'
        asset_node_id = 24 * 'b'

        # Uploading the file
        mock.add(responses.POST,
                 '%s/storage/stream/%s' % (self.endpoint, self.project_id),
                 json={
                     'status': 'ok',
                     'file_id': 24 * 'c',
                 },
                 status=201)

        # Finding the existing node
        mock.add(responses.GET,
                 '%s/nodes' % self.endpoint,
                 json={'_items': [{
                     '_id': asset_node_id,
                     '_etag': 'awesome-etag',
                     'name': 'test_nodes.py',
                     'node_type': 'asset',
                     'project': self.project_id,
                     'parent': parent_node_id,
                     'properties': {
                         'content_type': 'video',
                         'file': 24 * 'e',
                     }}
                 ]
                 })

        # Updating the node
        mock.add(responses.PUT,
                 '%s/nodes/%s' % (self.endpoint, asset_node_id),
                 json={'_created': 'Wed, 29 Jun 2016 14:45:35 GMT',
                       '_deleted': False,
                       '_etag': 'df983fb8834802be83f0f657201cbf7a3d177a9c',
                       '_id': asset_node_id,
                       '_status': 'OK',
                       '_updated': 'Tue, 05 Jul 2016 14:12:27 GMT'},
                 status=200)

        node = pillarsdk.Node.create_asset_from_file(
            project_id=self.project_id,
            parent_node_id=parent_node_id,
            asset_type='image',
            filename=__file__,
            api=self.api)

        self.assertEqual(node['_id'], asset_node_id)
        self.assertEqual(node['parent'], parent_node_id)
        self.assertEqual(node['name'], 'test_nodes.py')
        self.assertEqual(node['node_type'], 'asset')
        self.assertEqual(node['properties']['content_type'], 'image')
        self.assertEqual(node['properties']['file'], 24 * 'c')

    @mock.activate
    def test_create_asset_from_file__create_new_node(self):
        parent_node_id = 24 * 'a'
        asset_node_id = 24 * 'b'

        # Upload the file
        mock.add(responses.POST,
                 '%s/storage/stream/%s' % (self.endpoint, self.project_id),
                 json={
                     'status': 'ok',
                     'file_id': 24 * 'c',
                 },
                 status=201)

        # Try to find whether the node exists (it doesn't).
        mock.add(responses.GET,
                 '%s/nodes' % self.endpoint,
                 json={'_items': []})

        # Create a new node
        mock.add(responses.POST,
                 '%s/nodes' % self.endpoint,
                 json={
                     '_id': asset_node_id,
                     'parent': parent_node_id,
                     'name': 'test_nodes.py',
                     'project': self.project_id,
                 },
                 status=201)

        node = pillarsdk.Node.create_asset_from_file(
            project_id=self.project_id,
            parent_node_id=parent_node_id,
            asset_type='image',
            filename=__file__,
            api=self.api)

        self.assertEqual(node['_id'], asset_node_id)
        self.assertEqual(node['parent'], parent_node_id)
        self.assertEqual(node['name'], 'test_nodes.py')
        self.assertEqual(node['node_type'], 'asset')
        self.assertEqual(node['properties']['content_type'], 'image')
        self.assertEqual(node['properties']['file'], 24 * 'c')

    @mock.activate
    def test_create_asset_from_file__upload_fails(self):
        parent_node_id = 24 * 'a'

        # Upload the file
        mock.add(responses.POST,
                 '%s/storage/stream/%s' % (self.endpoint, self.project_id),
                 json={
                     'status': 'error',
                     'error': 'Internal server error'
                 },
                 status=500)

        self.assertRaises(
            sdk_exceptions.ServerError,
            pillarsdk.Node.create_asset_from_file,
            project_id=self.project_id,
            parent_node_id=parent_node_id,
            asset_type='image',
            filename=__file__,
            api=self.api)

    @mock.activate
    def test_create_asset_from_file__create_new_node_fails(self):
        parent_node_id = 24 * 'a'
        asset_node_id = 24 * 'b'

        # Upload the file
        mock.add(responses.POST,
                 '%s/storage/stream/%s' % (self.endpoint, self.project_id),
                 json={
                     'status': 'ok',
                     'file_id': 24 * 'c',
                 },
                 status=201)

        # Try to find whether the node exists (it doesn't).
        mock.add(responses.GET,
                 '%s/nodes' % self.endpoint,
                 json={'_items': []})

        # Create a new node, which fails
        mock.add(responses.POST,
                 '%s/nodes' % self.endpoint,
                 status=500)

        self.assertRaises(
            sdk_exceptions.ServerError,
            pillarsdk.Node.create_asset_from_file,
            project_id=self.project_id,
            parent_node_id=parent_node_id,
            asset_type='image',
            filename=__file__,
            api=self.api)

    @mock.activate
    def test_create_asset_from_file__update_existing_node_fails(self):
        parent_node_id = 24 * 'a'
        asset_node_id = 24 * 'b'

        # Uploading the file
        mock.add(responses.POST,
                 '%s/storage/stream/%s' % (self.endpoint, self.project_id),
                 json={
                     'status': 'ok',
                     'file_id': 24 * 'c',
                 },
                 status=201)

        # Finding the existing node
        mock.add(responses.GET,
                 '%s/nodes' % self.endpoint,
                 json={'_items': [{
                     '_id': asset_node_id,
                     '_etag': 'awesome-etag',
                     'name': 'test_nodes.py',
                     'node_type': 'asset',
                     'project': self.project_id,
                     'parent': parent_node_id,
                     'properties': {
                         'content_type': 'video',
                         'file': 24 * 'e',
                     }}
                 ]
                 })

        # Updating the node fails
        mock.add(responses.PUT,
                 '%s/nodes/%s' % (self.endpoint, asset_node_id),
                 status=500)

        self.assertRaises(
            sdk_exceptions.ServerError,
            pillarsdk.Node.create_asset_from_file,
            project_id=self.project_id,
            parent_node_id=parent_node_id,
            asset_type='image',
            filename=__file__,
            api=self.api)
