# -*- coding: utf8 -*-
import pickle
import unittest


class PickleTest(unittest.TestCase):
    def test_pickling_node(self):
        from pillarsdk import Node, File

        base_link = 'https://storage.googleapis.com/57534c07c37a7195f/_%2F326760eb7d7b244afe52'
        picture = File({
            '_id': '57534ccdc379cf1b24a7196d',
            '_created': '2016-06-04T23:49:01.000+0200',
            '_updated': '2017-09-07T15:55:37.000+0200',
            '_etag': '4bb3f525ea637c612fca882e5fcf334c056544fd',
            'status': 'complete',
            'name': '326763a705364fe0b0eb7d7b244afe52.png',
            'backend': 'gcs',
            'format': 'png',
            'variations': [
                {'width': 160,
                 'length': 4705,
                 'content_type': 'image/jpeg',
                 'height': 160,
                 'file_path': '326763a705364fe0b0eb7d7b244afe52-b.jpg',
                 'size': 'b',
                 'link': base_link + '-b.jpg'},
                {'width': 269,
                 'length': 8508,
                 'content_type': 'image/jpeg',
                 'height': 269,
                 'file_path': '326763a705364fe0b0eb7d7b244afe52-h.jpg',
                 'size': 'h',
                 'link': base_link + '-h.jpg'},
                {'width': 269,
                 'length': 8508,
                 'content_type': 'image/jpeg',
                 'height': 269,
                 'file_path': '326763a705364fe0b0eb7d7b244afe52-m.jpg',
                 'size': 'm',
                 'link': base_link + '-m.jpg'},
            ],
            'filename': '01d.png',
            'project': '57534c07c379cf1b24a7195f',
            'width': 269,
            'length': 9681,
            'user': '573dff22c379cf12e649f07a',
            'content_type': 'image/png',
            'height': 269,
            'file_path': '326763a705364fe0b0eb7d7b244afe52.png',
            'md5': '',
            'length_aggregate_in_bytes': 47050,
            'link': base_link + '.png',
            'link_expires': '2117-09-08T14:54:35.250+0200',
        }
        )
        parent_node = Node({
            '_id': '54134',
            'name': 'Dadday',
        })
        original = Node({
            '_id': '123456',
            'name': 'über cooole node',
            'parent': parent_node,
            'picture': picture,
        })

        pickled = pickle.dumps(original)
        restored = pickle.loads(pickled)

        self.assertEqual(restored._id, '123456')
        self.assertEqual(restored.name, 'über cooole node')

        self.assertIsInstance(restored.parent, Node)
        self.assertEqual(restored.parent.name, 'Dadday')

        url = 'https://storage.googleapis.com/57534c07c37a7195f/_%2F326760eb7d7b244afe52-m.jpg'
        self.assertEqual(url, original.picture.thumbnail('m'))
        self.assertEqual(url, restored.picture.thumbnail('m'))
