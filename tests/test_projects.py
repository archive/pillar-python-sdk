import unittest
import responses

import pillarsdk
import pillarsdk.exceptions as sdk_exceptions

mock = responses.RequestsMock(assert_all_requests_are_fired=True)


class ProjectsTests(unittest.TestCase):
    def setUp(self):
        self.endpoint = 'http://localhost:12345'
        self.api = pillarsdk.Api(
            endpoint=self.endpoint,
            username='',
            password='',
            token='jemoeder',
        )

    @mock.activate
    def test_find_project_happy(self):
        project_id = 24 * 'a'

        # Finding the existing project
        mock.add(responses.GET,
                 '%s/projects' % self.endpoint,
                 json={'_items': [{
                     '_id': project_id,
                     '_etag': 'awesome-etag',
                     'name': 'test-project'}
                 ]})

        proj = pillarsdk.Project.find_one({'_id': project_id}, api=self.api)
        self.assertEqual(project_id, proj['_id'])

    @mock.activate
    def test_find_project_unhappy_empty_response(self):
        project_id = 24 * 'a'

        # Finding the existing project
        mock.add(responses.GET,
                 '%s/projects' % self.endpoint,
                 json={})

        self.assertRaises(sdk_exceptions.ResourceNotFound,
                          pillarsdk.Project.find_one,
                          {'_id': project_id}, api=self.api)

    @mock.activate
    def test_find_project_unhappy_404_response(self):
        project_id = 24 * 'a'

        # Finding the existing project
        mock.add(responses.GET,
                 '%s/projects' % self.endpoint,
                 json={'_items': [{
                     '_id': project_id,
                     '_etag': 'awesome-etag',
                     'name': 'test-project'}
                 ]},
                 status=404)

        self.assertRaises(sdk_exceptions.ResourceNotFound,
                          pillarsdk.Project.find_one,
                          {'_id': project_id}, api=self.api)
