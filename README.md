# Pillar Python REST SDK
Integrate this module in your Python app to communicate with a Pillar server.

Supports Python 3.5-3.7.


## Development & Testing

Dependencies are managed via [Poetry](https://poetry.eustace.io/).

When starting development, run `poetry install` to create a virtual environment and install
dependencies.

To run the unit tests on all supported versions of Python, run `poetry run tox`.


## Caching

[Requests-Cache](https://requests-cache.readthedocs.org/) can be used to
cache HTTP requests. The Pillar Python REST SDK does not support it
directly, but provides the means to plug in different session objects:

    import requests_cache
    import pillarsdk

    req_sess = requests_cache.CachedSession(backend='sqlite',
                                            cache_name='blender_cloud')
    pillarsdk.Api.requests_session = req_sess

Any `pillarsdk.Api` instance will now use the cached session. To
temporary disable it, use:

    api = pillarsdk.Api.Default(endpoint="https://your.endpoint")
    with api.requests_session.cache_disabled():
        node = pillarsdk.Node.find('1234')
