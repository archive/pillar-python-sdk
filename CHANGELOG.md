Pillar Python SDK changelog
===========================

## Version 1.10 (2019-05-31)

- Added `User.patch()`. This is used to set the user's username.
- Deprecated the use of Gravatar in favour of Pillar-hosted avatars.


## Version 1.9 (2019-05-28)

- Updated the dependencies to current versions.


Version 1.8 (2019-05-14)
----------------------------

- Switched to [Poetry](https://poetry.eustace.io/) for dependency management.


Version 1.7 (2018-09-17)
----------------------------

- Added support pickling/unpickling resources.
- Added support for `datetime` objects in API requests.
- Added support for Python 3.7


Version 1.6
-----------

- Allow PATCHing nodes.
- Always verify SSL connections.
- Added Project.find_by_url(project_url) because it's so commonly used as identifier.


Version 1.5
-----------

- Using HTTP OPTIONS call for Project.node_type_has_method()
- Enable global HTTP headers to be sent with every request.
- Prevented error when response doesn't have `_items`.
- Don't gobble up the exceptions.BadRequest exception


Version 1.4
-----------

- Added pillarsdk.utils.is_valid_id(some_id) function.
- Datetimes are now returned as timezone-aware.
- Ported utils.remove_private_keys() from Pillar
- Added Node.create_asset_from_file() utility function
- create_asset_from_file: allow parent_node_id=None
- create_asset_from_file: added extra_where param to limit updatable nodes.
- Split Node.create_asset_from_file and added direct uploading from file obj
- Removed the obsolete NodeType class.
- Added Node.share() and Node.get_share_links() methods.
- Node: removed 'theatre_link'
- Added exception for 413 Request Entity Too Large


Version 1.3
-----------

- Added User.me() and Find.find_from_endpoint() class methods.
- Expose Resource class at top module level
- Added exception for HTTP 412/Precondition Failed
- Added Resource.new(dict_or_resource) class method (allows upcasting).
- Force projection of Project.category and Project.user (required for home project).
- Also expose ForbiddenAccess exception
- Remove testing on Python 3.4, as Ubuntu's default 3.x is now 3.5.


Version 1.2
-----------

- Removed File.thumbnail_file in favour of File.thumbnail
- Add URL in error messages.
- Added List.all_from_endpoint(url) function.


Version 1.1
-----------

- Added project user management.
- Project creation uses /p/create entry point on Pillar
- Bug fixes.
- Files: Get thumbnails from variations, rather than from 'link'
  property
- Made "'field' in resource" work


Version 1.0
-----------

- Initial public release for use with Blender Cloud addon.
- Fixed compatibility with clients not using a subclient token.
